package com.example.yuri.systemalerwindow;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by test on 2/8/16.
 */
public class BackgroundService extends Service {
    private final static String TAG = "Service";
    private int speed=25;
    private ImageView imageView;
    private WindowManager wm;
    private Point screenSize;
    private Handler handler;
    private Random random = new Random();
    enum Moving {UP, DOWN, LEFT, RIGHT}
    private Moving mMove;
    private WindowManager.LayoutParams params;
    float x,y;

    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            //Log.d(TAG, "Step: " +mMove.toString());
            x=params.x;
            y=params.y;

                switch (mMove) {
                    case UP:
                        x--;
                        y--;
                        break;
                    case DOWN:
                        x++;
                        y++;
                        break;
                    case LEFT:
                        x++;
                        y--;
                        break;
                    case RIGHT:
                        x--;
                        y++;
                        break;
            }

            if (x<=0 || y<=0 || x+imageView.getWidth()>=screenSize.x || y+imageView.getHeight()>=screenSize.y) {
                mMove=Moving.values()[random.nextInt(4)];
                speed = 10+random.nextInt(15);
            } else {
                params.x = (int) x;
                params.y = (int) y;
                wm.updateViewLayout(imageView, params);
            }

            handler.postDelayed(mRun, speed);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.i(TAG, "Service: onStartCommand");
        toScreen();
        return super.onStartCommand(intent, flags, startId);
    }

    private void toScreen(){
        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 0;

        wm = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        screenSize= new Point(displaymetrics.widthPixels, displaymetrics.heightPixels);

        imageView = new ImageView(getApplicationContext());
        imageView.setImageResource(R.mipmap.tellit);

        wm.addView(imageView, params);

        imageView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(getApplicationContext(), "Tellit!", Toast.LENGTH_SHORT).show();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX
                                + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY
                                + (int) (event.getRawY() - initialTouchY);
                        wm.updateViewLayout(imageView, params);
                        return true;
                }
                return false;
            }
        });

        handler = new Handler();
        mMove = Moving.RIGHT;
        handler.postDelayed(mRun, speed);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WindowManager wm = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        wm.removeView(imageView);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        //Log.i(TAG, "Service: onTaskRemoved");
        restartMyApp(TimeUnit.SECONDS.toMillis(3));
    }

    private void restartMyApp(long time) {
        PendingIntent myActivity = PendingIntent.getActivity(getApplicationContext(),
                192837, new Intent(getApplicationContext(), MainActivity.class),
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager;
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC,
                System.currentTimeMillis() + time, myActivity );
    }
}